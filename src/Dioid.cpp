#include "Dioid.hpp"

Interval::Interval(float min, float max) {
  this->m_min = min;
  this->m_max = max;
}

float Interval::min() { return m_min; }

float Interval::max() { return m_max; }

bool Interval::has(float value) { return value <= max() && value >= min(); }

Dioid::Dioid(Interval interval, float (*plus)(float, float), float neutral_plus,
             float (*times)(float, float), float neutral_times)
    : interval(interval) {
  this->m_plus = plus;
  this->m_neutral_plus = neutral_plus;
  this->m_times = times;
  this->m_neutral_times = neutral_times;
}

float Dioid::plus(float a, float b) { return (*m_plus)(a, b); }

float Dioid::times(float a, float b) { return (*m_times)(a, b); }

float Dioid::neutral_plus() { return m_neutral_plus; }

float Dioid::neutral_times() { return m_neutral_times; }
