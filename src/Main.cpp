#include <iostream>
#include "Graph.hpp"
#include "Matrix.hpp"


int main()
{
	// Declaring graph G
	Graph<int> g = Graph<int>();

	// Adding vertices
	g.add(1);
	g.add(2);
	g.add(3);
	g.add(4);

	// Adding edges
	g.join(1, 2);
	g.join(2, 4);
	g.join(3, 1);
	g.join(3, 4);
	g.join(1, 1);
	g.join(2, 3);

	// Printing the graph in the standard output
	printGraph(g);

	// Declaring matrices
	Matrix<Edge<int> *> adjacencyM = g.adjacencyMatrix();
	Matrix<bool> boolM = g.booleanMatrix();
	Matrix<int> incidencyM = g.incidencyMatrix();

	// Printing matrices
	std::cout << "\nAdjacency matrix:" << std::endl;
	printMatrix(&adjacencyM);
	std::cout << "\nBoolean matrix:" << std::endl;
	printMatrix(&boolM);
	std::cout << "\nIncidency matrix:" << std::endl;
	printMatrix(&incidencyM);

	// Declaring paths
	Graph<int>::Path pathA = Graph<int>::Path();
	pathA.add(nullptr);

	// End of program
	return 0;
}
