#---------------------------------------------------------------------------#
# PROJECT STRUCTURE, SOURCES FILES AND OBJECTS								#
#---------------------------------------------------------------------------#

# Project's folder structure
BINARY_DIRECTORY	:= bin
INCLUDE_DIRECTORY	:= include
LIBRARY_DIRECTORY	:= lib
OBJECTS_DIRECTORY	:= obj
RESOURCES_DIRECTORY	:= res
SOURCE_DIRECTORY	:= src

# Sources and objects files
SOURCES				:= $(wildcard $(SOURCE_DIRECTORY)/*.cpp) $(wildcard $(SOURCE_DIRECTORY)/**/*.cpp) $(wildcard $(SOURCE_DIRECTORY)/**/**/*.cpp)
OBJECTS				:= $(subst $(SOURCE_DIRECTORY),$(OBJECTS_DIRECTORY),$(SOURCES:.cpp=.o))

# Directories that are required, but may not exist yet
GENERATED_OBJECTS_DIRS	:= $(subst $(SOURCE_DIRECTORY),$(OBJECTS_DIRECTORY),$(dir $(OBJECTS)))

#---------------------------------------------------------------------------#
# ENVIRONMENT, COMPILER AND FLAGS, PLATFORM	PORTABILITY						#
#---------------------------------------------------------------------------#

# Compiler, executable and flags
CC			:= g++
EXEC		:= graph
# -Wall		: Activate all warnings
# -Wextra	: Activate special warnings
# -Werror	: Makes the warnings become errors and stop compilation
WARNINGS	:= -Wall -Wextra
# -std=c11	: Make the compiler use the c11 standard for compilation
FLAGS		:= -g $(WARNINGS) -std=c++11
INCLUDES	:= -I$(INCLUDE_DIRECTORY)/
LDFLAGS		:= -L$(LIBRARY_DIRECTORY)/
# Link SDL2 library files
LIBS		:=


# For Windows NT systems (at least)
ifeq ($(OS), Windows_NT)
CMDSEP		:= &
EXT			:= .exe
MAKEDIR		:= mkdir
REMOVE		:= del /S
SEPARATOR	:= \\

else
CMDSEP		:= ;
EXT			:=
MAKEDIR		:= mkdir -p
REMOVE		:= rm -f
SEPARATOR	:= /

endif

# Add the SDL2 links at the and of the linker flags
OUTPUT		:= $(BINARY_DIRECTORY)$(SEPARATOR)$(EXEC)$(EXT)


#---------------------------------------------------------------------------#
# TARGETS																	#
#---------------------------------------------------------------------------#

# Building targets

all: gen-dirs $(EXEC)

$(OBJECTS_DIRECTORY)/%.o: $(SOURCE_DIRECTORY)/%.cpp
	$(CC) $(FLAGS) -c $< -o $@ $(INCLUDES)

$(EXEC): $(OBJECTS)
	$(CC) $(FLAGS) $(OBJECTS) -o $(OUTPUT) $(LDFLAGS) $(LIBS)

# Build the project by creating all directories manually
gen-dirs:
	$(MAKEDIR) $(BINARY_DIRECTORY)
	$(foreach d,$(GENERATED_OBJECTS_DIRS),$(MAKEDIR) $(subst /,$(SEPARATOR),$(d)) $(CMDSEP))

debug-info:
	@echo "Sources files are: $(SOURCES)"
	@echo "Objects files are: $(OBJECTS)"
	@echo "Required directories are: $(GENERATED_OBJECTS_DIRS)"

run:
	$(BINARY_DIRECTORY)/$(EXEC)


# Cleaning targets

# Clean the project by removing every object files
clean:
	$(foreach obj,$(OBJECTS:/=$(SEPARATOR)),$(REMOVE) $(obj) $(CMDSEP))

# Make the project even cleaner by removing the executable
mrproper: clean
	$(REMOVE) $(OUTPUT)
