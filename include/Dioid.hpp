#ifndef DIOID_DEF
#define DIOID_DEF

class Interval {
private:
  float m_min;
  float m_max;

public:
  Interval(float min, float max);
  float min();
  float max();
  bool has(float value);
};

class Dioid {
private:
  Interval interval;
  float (*m_plus)(float, float);
  float m_neutral_plus;
  float (*m_times)(float, float);
  float m_neutral_times;

public:
  Dioid(Interval interval, float (*plus)(float, float), float meutral_plus,
        float (*times)(float, float), float neutral_times);

  float plus(float a, float b);
  float times(float a, float b);
  float neutral_plus();
  float neutral_times();
};

#endif
