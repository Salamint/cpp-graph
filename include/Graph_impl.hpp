#include <set>
#include "Graph.hpp"
#include "Edge.hpp"
#include "Matrix.hpp"

template <typename T> Graph<T>::Graph(bool directed)
{
	this->directed = directed;
	this->vertices = std::set<T>();
	this->edges = std::set<Edge<T> *>();
}

template <typename T> Graph<T>::~Graph()
{
	typename std::set<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		delete *it;
	}
}

template <typename T> std::set<T> const * Graph<T>::getVertices() const
{
	return &vertices;
}

template <typename T> std::set<Edge<T> *> const * Graph<T>::getEdges() const
{
	return &edges;
}

template <typename T> bool Graph<T>::areJoined(T head, T tail) const
{
	return edges.find(&Edge<T>(head, tail, isDirected())) != edges.end();
}

template <typename T> bool Graph<T>::contains(T vertex) const
{
	return vertices.find(vertex) != vertices.end();
}

template <typename T> bool Graph<T>::isDirected() const
{
	return directed;
}

template <typename T> bool Graph<T>::isEmpty() const
{
	return vertices.empty();
}

template <typename T> bool Graph<T>::isSimple() const
{
	return multiplicity() <= 1;
}

template <typename T> unsigned int Graph<T>::multiplicity() const
{
	typename std::set<Edge<T> *>::iterator e, f;
	unsigned int multiplicity = 0, counter;

	for (e = edges.begin(); e != edges.end(); e++)
	{
		counter = 0;
		for (f = edges.begin(); f != edges.end(); f++)
		{
			if ((**e) == (**f))
			{
				counter++;
			}
		}

		if (counter > multiplicity)
		{
			multiplicity = counter;
		}
	}

	return multiplicity;
}

template <typename T> unsigned int Graph<T>::numberOfEdges() const
{
	return edges.size();
}

template <typename T> unsigned int Graph<T>::numberOfVertices() const
{
	return vertices.size();
}

template <typename T> Matrix<Edge<T> *> Graph<T>::adjacencyMatrix() const
{
	unsigned int i, j;
	typename std::set<T>::iterator v, w;
	typename std::set<Edge<T> *>::iterator e;
	Matrix<Edge<T> *> matrix = Matrix<Edge<T> *>(numberOfVertices(), numberOfVertices(), nullptr);

	i = 0;
	for (v = vertices.begin(); v != vertices.end(); v++)
	{
		j = 0;
		for (w = vertices.begin(); w != vertices.end(); w++)
		{
			for (e = edges.begin(); e != edges.end(); e++)
			{
				if ((*e)->getHead() == (*v) && (*e)->getTail() == (*w))
				{
					matrix.setValue(i, j, (*e));
				}
			}
			j++;
		}
		i++;
	}

	return matrix;
}

template <typename T> Matrix<bool> Graph<T>::booleanMatrix() const
{
	unsigned int i, j;
	typename std::set<T>::iterator v, w;
	typename std::set<Edge<T> *>::iterator e;
	Matrix<bool> matrix = Matrix<bool>(numberOfVertices(), numberOfVertices(), false);

	i = 0;
	for (v = vertices.begin(); v != vertices.end(); v++)
	{
		j = 0;
		for (w = vertices.begin(); w != vertices.end(); w++)
		{
			for (e = edges.begin(); e != edges.end(); e++)
			{
				if ((*e)->getHead() == (*v) && (*e)->getTail() == (*w))
				{
					matrix.setValue(i, j, true);
				}
			}
			j++;
		}
		i++;
	}

	return matrix;
}
template <typename T> Matrix<int> Graph<T>::incidencyMatrix() const
{
	unsigned int i, j;
	typename std::set<T>::iterator v;
	typename std::set<Edge<T> *>::iterator e;
	Matrix<int> matrix = Matrix<int>(numberOfVertices(), numberOfEdges(), 0);

	i = 0;
	for (v = vertices.begin(); v != vertices.end(); v++)
	{
		j = 0;
		for (e = edges.begin(); e != edges.end(); e++)
		{
			if ((*v) == (*e)->getHead())
			{
				matrix.setValue(i, j, 1);
			}
			else if ((*v) == (*e)->getTail())
			{
				matrix.setValue(i, j, -1);
			}
			j++;
		}
		i++;
	}
	return matrix;
}

template <typename T> unsigned int Graph<T>::degree() const
{
	return innerDegree() + outerDegree();
}

template <typename T> unsigned int Graph<T>::innerDegree() const
{
	typename std::set<T>::iterator v;
	unsigned int degree = 0;
	for (v = vertices.begin(); v != vertices.end(); v++)
	{
		degree += vertexInnerDegree(*v);
	}
	return degree;
}

template <typename T> unsigned int Graph<T>::outerDegree() const
{
	typename std::set<T>::iterator v;
	unsigned int degree = 0;
	for (v = vertices.begin(); v != vertices.end(); v++)
	{
		degree += vertexOuterDegree(*v);
	}
	return degree;
}

template <typename T> unsigned int Graph<T>::vertexDegree(T vertex) const
{
	return vertexInnerDegree(vertex) + vertexOuterDegree(vertex);
}

template <typename T> unsigned int Graph<T>::vertexInnerDegree(T vertex) const
{
	return edgesWithHead(vertex);
}

template <typename T> unsigned int Graph<T>::vertexOuterDegree(T vertex) const
{
	return edgesWithTail(vertex);
}

template <typename T> void Graph<T>::add(T vertex)
{
	vertices.insert(vertex);
}

template <typename T> void Graph<T>::join(T head, T tail)
{
	if (contains(head) && contains(tail))
	{
		edges.insert(new Edge<T>(head, tail, isDirected()));
	}
}

template <typename T> void Graph<T>::remove(T vertex)
{
	vertices.erase(vertex);
	removeEdgesWithVertex(vertex);
}

template <typename T> void Graph<T>::unjoin(T head, T tail)
{
	Edge<T> *edge = new Edge<T>(head, tail, isDirected());
	typename std::set<Edge<T> *>::iterator it;
	if (areJoined(head, tail))
	{
		for (it = edges.begin(); it != edges.end(); it++)
		{
			if (**it == *edge)
			{
				edges.erase(*it);
				delete *it;
			}
		}
		edges.erase(Edge<T>(head, tail, isDirected()));
	}
	delete edge;
}

template <typename T> std::set<Edge<T> *> Graph<T>::edgesWithHead(T vertex) const
{
	typename std::set<Edge<T> *>::iterator it;
	typename std::set<Edge<T> *> edgesWithHead = std::set<Edge<T> *>();
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if ((*it)->getHead() == vertex)
		{
			edgesWithHead.insert(*it);
		}
	}
	return edgesWithHead;
}

template <typename T> std::set<Edge<T> *> Graph<T>::edgesWithTail(T vertex) const
{
	typename std::set<Edge<T>>::iterator it;
	typename std::set<Edge<T> *> edgesWithTail = std::set<Edge<T> *>();
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if ((*it)->getTail() == vertex)
		{
			edgesWithTail.insert(*it);
		}
	}
	return edgesWithTail;
}

template <typename T> void Graph<T>::removeEdgesWithVertex(T vertex)
{
	typename std::set<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if ((*it)->hasVertex(vertex))
		{
			unjoin((*it)->getHead(), (*it)->getTail());
		}
	}
}

template <typename T> void printGraph(Graph<T> const &graph)
{
	std::set<T> const *vertices = graph.getVertices();
	std::set<Edge<T> *> const *edges = graph.getEdges();
	typename std::set<T>::iterator itV;
	typename std::set<Edge<T> *>::iterator itE;
	std::cout << "G = (S, A), |S| = " << graph.numberOfVertices() << ", |A| = " << graph.numberOfEdges() << std::endl;

	std::cout << "S = { ";
	for (itV = vertices->begin(); itV != vertices->end(); itV++)
	{
		if (itV != vertices->begin())
		{
			std::cout << ", ";
		}
		std::cout << (*itV);
	}
	std::cout << " }" << std::endl;

	std::cout << "A = { ";
	for (itE = edges->begin(); itE != edges->end(); itE++)
	{
		if (itE != edges->begin())
		{
			std::cout << ", ";
		}
		std::cout << (**itE);
	}
	std::cout << " }" << std::endl;

	std::cout << "G is a ";

	if (graph.isDirected())
	{
		std::cout << "directed ";
	}
	else
	{
		std::cout << "undirected ";
	}

	if (graph.isEmpty())
	{
		std::cout << "empty graph";
	}
	else if (graph.isSimple())
	{
		std::cout << "simple graph";
	}
	else
	{
		std::cout << graph.multiplicity() << "-graph";
	}

	std::cout << "." << std::endl;
}
