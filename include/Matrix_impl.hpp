#include <iostream>
#include "Matrix.hpp"


template <typename T> Matrix<T>::Matrix(unsigned int rows, unsigned int columns, T value)
{
	this->rows = rows;
	this->columns = columns;
	this->values = new T * [rows];
	for (unsigned int i = 0; i < rows; i++)
	{
		this->values[i] = new T[columns];
	}
	setAllValues(value);
}

template <typename T> Matrix<T>::~Matrix()
{
	for (unsigned int i = 0; i < getRows(); i++)
	{
		delete [] values[i];
	}
	delete [] values;
}

template <typename T> unsigned int Matrix<T>::getColumns() const
{
	return columns;
}

template <typename T> unsigned int Matrix<T>::getRows() const
{
	return rows;
}

template <typename T> T Matrix<T>::getValue(unsigned int row, unsigned int column) const
{
	if (row < getRows() && column < getColumns())
	{
		return values[row][column];
	}
	return values[getRows() - 1][getColumns() - 1];
}

template <typename T> void Matrix<T>::setAllValues(T value)
{
	for (unsigned int i = 0; i < getRows(); i++)
	{
		for (unsigned int j = 0; j < getColumns(); j++)
		{
			setValue(i, j, value);
		}
	}
}

template <typename T> void Matrix<T>::setValue(unsigned int row, unsigned int column, T value)
{
	if (row < getRows() && column < getColumns())
	{
		values[row][column] = value;
	}
}

template <typename T> void printMatrix(Matrix<T> const *matrix)
{
	unsigned int i, j;
	for (i = 0; i < matrix->getRows(); i++)
	{
		if (i == 0)
		{
			std::cout << "/";
		}
		else if (i == matrix->getRows() - 1)
		{
			std::cout << "\\";
		}
		else
		{
			std::cout << "|";
		}
		std::cout << " ";

		for (j = 0; j < matrix->getColumns(); j++)
		{
			if (j == 0)
			{
				std::cout << matrix->getValue(i, j);
			}
			else
			{
				std::cout << ", " << matrix->getValue(i, j);
			}
		}

		std::cout << " ";
		if (i == 0)
		{
			std::cout << "\\";
		}
		else if (i == matrix->getRows() - 1)
		{
			std::cout << "/";
		}
		else
		{
			std::cout << "|";
		}
		std::cout << std::endl;
	}
}
