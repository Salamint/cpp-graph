#ifndef MATRIX_DEF
#define MATRIX_DEF

template <typename T>
class Matrix
{
private:
	unsigned int columns;
	unsigned int rows;
	T **values;

public:
	Matrix(unsigned int rows, unsigned int columns, T value);
	~Matrix();

	unsigned int getColumns() const;
	unsigned int getRows() const;
	T getValue(unsigned int row, unsigned int column) const;

	void setAllValues(T value);
	void setValue(unsigned int row, unsigned int column, T value);
};

template <typename T> void printMatrix(Matrix<T> const *matrix);

#include "Matrix_impl.hpp"

#endif
