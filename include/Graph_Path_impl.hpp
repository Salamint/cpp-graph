#include <list>
#include <Graph.hpp>
#include <set>
#include "Edge.hpp"

template <typename T> Graph<T>::Path::Path() {}

template <typename T> Edge<T> * Graph<T>::Path::getEdge(unsigned int index) const
{
	unsigned int i = 0;
	typename std::list<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if (i == index)
		{
			return (*it);
		}
	}
	return nullptr;
}

template <typename T> Edge<T> * Graph<T>::Path::getHead() const
{
	return *edges.begin();
}

template <typename T> Edge<T> * Graph<T>::Path::getTail() const
{
	typename std::list<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if (it + 1 == edges.end())
		{
			return (*it);
		}
	}
	return nullptr;
}

template <typename T> bool Graph<T>::Path::isSimple() const
{
	std::set<Edge<T> *> unique_edges = std::set<Edge<T> *>();
	typename std::list<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if (unique_edges.find(*it) != unique_edges.end())
		{
			return false;
		}
		else
		{
			unique_edges.insert(*it);
		}
	}
	return true;
}

template <typename T> bool Graph<T>::Path::isElementary() const
{
	std::set<T> vertices = std::set<T>();
	typename std::list<Edge<T> *>::iterator it;
	for (it = edges.begin(); it != edges.end(); it++)
	{
		if (vertices.find((*it)->getHead()) != vertices.end())
		{
			return false;
		}
		else
		{
			vertices.insert((*it)->getHead());
		}
	}
	return vertices.find((*it)->getTail()) == vertices.end();
}

template <typename T> unsigned int Graph<T>::Path::size() const
{
	return edges.size();
}

template <typename T> void Graph<T>::Path::add(Edge<T> *edge)
{
	if (getTail()->getTail() != edge->getHead())
	{
		edges.insert(edge);
	}
}
