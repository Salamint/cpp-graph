#include "Edge.hpp"


template <typename T> Edge<T>::Edge(T head, T tail, bool directed)
{
	this->head = head;
	this->tail = tail;
	this->directed = directed;
}

template <typename T> bool Edge<T>::isDirected() const
{
	return directed;
}

template <typename T> T Edge<T>::getHead() const
{
	return head;
}

template <typename T> T Edge<T>::getTail() const
{
	return tail;
}

template <typename T> bool Edge<T>::hasVertex(T vertex) const
{
	return vertex == getHead() || vertex == getTail();
}

template <typename T> bool operator==(Edge<T> const &e1, Edge<T> const &e2)
{
	return e1.getHead() == e2.getHead() && e1.getTail() == e2.getTail();
}


template <typename T> bool operator<(const Edge<T> e1, const Edge<T> e2)
{
	return e1.getHead() < e2.getHead() || e1.getTail() < e2.getTail();
}

template <typename T> std::ostream & operator<<(std::ostream &stream, const Edge<T> &edge)
{
	stream << "(" << edge.getHead() << ", " << edge.getTail() << ")";
	return stream;
}
