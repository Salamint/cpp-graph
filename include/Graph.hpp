#ifndef GRAPH_DEF
#define GRAPH_DEF

#include <list>
#include <ostream>
#include <set>

#include "Edge.hpp"
#include "Matrix.hpp"



template <typename T>
class Graph
{
private:
	bool directed;
	std::set<T> vertices;
	std::set<Edge<T> *> edges;

protected:
	std::set<Edge<T> *> edgesWithHead(T vertex) const;
	std::set<Edge<T> *> edgesWithTail(T vertex) const;
	void removeEdgesWithVertex(T vertex);

public:
	Graph(bool directed = true);
	~Graph();

	std::set<T> const * getVertices() const;
	std::set<Edge<T> *> const * getEdges() const;

	bool areJoined(T head, T tail) const;
	bool contains(T vertex) const;
	bool isDirected() const;
	bool isEmpty() const;
	bool isSimple() const;

	unsigned int multiplicity() const;
	unsigned int numberOfEdges() const;
	unsigned int numberOfVertices() const;

	Matrix<Edge<T> *> adjacencyMatrix() const;
	Matrix<bool> booleanMatrix() const;
	Matrix<int> incidencyMatrix() const;

	unsigned int degree() const;
	unsigned int innerDegree() const;
	unsigned int outerDegree() const;

	unsigned int vertexDegree(T vertex) const;
	unsigned int vertexInnerDegree(T vertex) const;
	unsigned int vertexOuterDegree(T vertex) const;

	void add(T vertex);
	void join(T head, T tail);
	void remove(T vertex);
	void unjoin(T head, T tail);

	class Path
	{
	private:
		std::list<Edge<T> *> edges;

	public:
		Path();

		Edge<T> * getEdge(unsigned int index) const;
		Edge<T> * getHead() const;
		Edge<T> * getTail() const;

		bool isSimple() const;
		bool isElementary() const;

		unsigned int size() const;

		void add(Edge<T> *edge);
	};
};

template <typename T> void printGraph(Graph<T> const &graph);

template <typename T> std::ostream & operator<<(std::ostream &stream, typename Graph<T>::Path const &path);

#include "Graph_impl.hpp"
#include "Graph_Path_impl.hpp"

#endif
