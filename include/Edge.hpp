#ifndef EDGE_DEF
#define EDGE_DEF

#include <ostream>
template <typename T>
class Edge
{
private:
	bool directed;
	T head;
	T tail;

public:
	Edge(T head, T tail, bool directed);

	bool isDirected() const;
	T getHead() const;
	T getTail() const;
	bool hasVertex(T vertex) const;
};

template <typename T> bool operator==(Edge<T> const &e1, Edge<T> const &e2);
template <typename T> bool operator<(const Edge<T> e1, const Edge<T> e2);
template <typename T> std::ostream & operator<<(const std::ostream &stream, const Edge<T> &edge);

#include "Edge_impl.hpp"

#endif
