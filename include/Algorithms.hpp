#ifndef ALGO_DEF
#define ALGO_DEF

#include <limits>

#define INFINITY std::numeric_limits<int>::max()

class Magrange {};

class Warshall {};

class FordBellman {};

#endif
